---
layout: page
title: "People: Paul Polman"
permalink: /people/paul-polman
---

## Timeline

- 1979 - 2006: [Procter & Gamble](/organizations/procter-and-gamble)
- 2006 - 2009: [Nestle](/organizations/nestle) - CFO + Head of Americas
- 2009 - Present: [Unilever](/organizations/unilever) - CEO

## Sources

1. [Wikipedia](https://en.wikipedia.org/wiki/Paul_Polman)
