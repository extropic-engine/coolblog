---
layout: page
title: Organizations
permalink: /organizations/
---

# Public Corporations

Includes société anonyme, naamloze vennootschap, etc.

- [3M](3m-company) (Public)
- [Nestle](nestle) (SA)
- [The New York Times Company](new-york-times-company) (Public)
- [Procter & Gamble](procter-and-gamble) (Public)
- [Unilever](unilever) (Public/NV)
- - [Second Generation](second-generation) (subsidiary)
- [The Woolbridge Company](woolbridge-company)

# Lobbying / Advocacy

- [World Business Council for Sustainable Development](world-business-council-for-sustainable-development)
