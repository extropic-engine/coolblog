---
layout: page
title: "Organizations: Unilever"
permalink: /organizations/unilever
---

Ownership: NV/Public (NYSE: UN, UL)

## Leadership

### CEO

- 2009-Present: [Paul Polman](/people/paul-polman)

## Subsidiaries

- [Second Generation](/organizations/second-generation)
