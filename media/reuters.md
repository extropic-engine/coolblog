---
layout: page
title: "Media: Reuters"
permalink: /media/reuters
---

Ownership: Thomson Reuters (public) -> [The Woolbridge Company](organizations/woolbridge-company) (majority shareholder) -> [Thomson Family](people/david-thomson)
